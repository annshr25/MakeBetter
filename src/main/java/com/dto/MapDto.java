package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapDto {
    private ArrayList<Long> idArray;
    private ArrayList<String> ratingArray;
}
