package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppealDto {
    private Integer appealId;
    private String category;
    private String description;
    private String coords;
    private String address;
    private String imgPath;
    private Long userId;
}
