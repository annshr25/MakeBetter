package com.service;

import com.dao.AppealDao;
import com.dao.RegistrationDao;
import com.dto.AppealDto;
import com.dto.MapDto;
import com.entity.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;


@Service
public class AppealService {
    private AppealDao appealDao;
    private RegistrationDao registrationDao;
    private CustomUserDetailsService customUserDetailsServicel;

    @Autowired
    public AppealService(AppealDao appealDao, RegistrationDao registrationDao, CustomUserDetailsService customUserDetailsServicel) {
        this.appealDao = appealDao;
        this.registrationDao = registrationDao;
        this.customUserDetailsServicel = customUserDetailsServicel;
    }

    @Transactional
    public boolean addAppeal(AppealDto appealDto) {
        MyUser user = customUserDetailsServicel.getCurrentUser();
        appealDto.setUserId(user.getId());

        UserAppeal userAppeal = new UserAppeal();
        BeanUtils.copyProperties(appealDto, userAppeal);
        userAppeal.setRating((long) 0);

        user.getUserAppealList().add(userAppeal);
        userAppeal.setUser(user);
        registrationDao.update(user);

        return appealDao.create(userAppeal);
    }

    @Transactional
    public Integer getLastId() {
        return appealDao.getLastId();
    }

    @Transactional
    public Response getAppeals() {
        Response response = new Response();
        for (UserAppeal userAppeal : appealDao.getAll()) {

            JsonItem jsonItem = new JsonItem();
            jsonItem.setId(userAppeal.getId());

            PropertiesItem propertiesItem = new PropertiesItem();
            propertiesItem.setBalloonContentHeader("" + userAppeal.getId());
            propertiesItem.setBalloonContentBody(userAppeal.getDescription() + "\n Адрес обращения: " + userAppeal.getAddress());
            propertiesItem.setBalloonContentFooter("" + userAppeal.getRating());

            propertiesItem.setIdUser(userAppeal.getUser().getId());
            propertiesItem.setIdCurrent(customUserDetailsServicel.getCurrentUser().getId());

            GeometryItem geometryItem = new GeometryItem();
            String[] arr = userAppeal.getCoords().split(",");
            double[] coordinates = {Double.parseDouble(arr[0]), Double.parseDouble(arr[1])};
            geometryItem.setCoordinates(coordinates);

            jsonItem.setGeometry(geometryItem);
            jsonItem.setProperties(propertiesItem);
            response.getFeatures().add(jsonItem);
        }
        return response;
    }

    @Transactional
    public void updateRating(MapDto mapdto) {
        ArrayList<Long> idArr = mapdto.getIdArray();
        ArrayList<String> ratingArr = mapdto.getRatingArray();
        for (Long id : idArr) {
            UserAppeal updateUserAppeal = appealDao.getById(id);
            updateUserAppeal.setRating(Long.parseLong(ratingArr.get(idArr.indexOf(id))));
            appealDao.update(updateUserAppeal);
        }
    }
}
