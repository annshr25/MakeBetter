package com.service;

import com.dao.ChatRoomDao;
import com.entity.ChatRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ChatRoomService {

    private ChatRoomDao chatRoomDao;

    @Autowired
    public ChatRoomService(ChatRoomDao chatRoomDao) {
        this.chatRoomDao = chatRoomDao;
    }

    @Transactional
    public String getChatId(
            String senderId, String recipientId, boolean createIfNotExist) {

        List<ChatRoom> chatRoomList = chatRoomDao.findBySenderIdAndRecipientId(senderId, recipientId);

        if (!chatRoomList.isEmpty()) {
            return chatRoomList.get(0).getChatId();
        } else {
            if (!createIfNotExist) {
                return null;
            } else {
                String chatId =
                        String.format("%s_%s", senderId, recipientId);

                ChatRoom senderRecipient = ChatRoom
                        .builder()
                        .chatId(chatId)
                        .senderId(senderId)
                        .recipientId(recipientId)
                        .build();

                ChatRoom recipientSender = ChatRoom
                        .builder()
                        .chatId(chatId)
                        .senderId(recipientId)
                        .recipientId(senderId)
                        .build();

                chatRoomDao.save(senderRecipient);
                chatRoomDao.save(recipientSender);

                return chatId;
            }
        }
    }
}