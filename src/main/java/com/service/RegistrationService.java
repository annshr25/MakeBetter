package com.service;

import com.dao.RegistrationDao;
import com.dto.UserDto;
import com.entity.MyUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class RegistrationService {
    private static final Logger log = LoggerFactory.getLogger(RegistrationService.class);

    private RegistrationDao registrationDao;
    PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationService(RegistrationDao registrationDao, PasswordEncoder passwordEncoder) {
        this.registrationDao = registrationDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public boolean register(UserDto userDto) {
        MyUser myUser = new MyUser();
        BeanUtils.copyProperties(userDto, myUser);
        myUser.setPassword(passwordEncoder.encode(myUser.getPassword()));
        myUser.setRole("USER");
        MyUser userFromDB = registrationDao.findUserByLogin(myUser.getUsername());
        if (userFromDB != null) {
            return false;
        }
        return registrationDao.create(myUser);
    }

    @Transactional
    public MyUser findUserById(Long id) {
        return registrationDao.getById(id);
    }
}
