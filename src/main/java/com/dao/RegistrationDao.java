package com.dao;

import com.entity.MyUser;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class RegistrationDao implements GenericDao<MyUser, Long> {

    private SessionFactory sessionFactory;

    @Autowired
    public RegistrationDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public MyUser findUserByLogin(String username) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<MyUser> criteria = builder.createQuery(MyUser.class);//создание объекта запроса
        Root<MyUser> root = criteria.from(MyUser.class);//запрос
        ParameterExpression<String> nameParam = builder.parameter(String.class);
        criteria.select(root).where(builder.equal(root.get("username"), nameParam));//указан тип запроса

        Query<MyUser> query = session.createQuery(criteria);
        query.setParameter(nameParam, username);
        MyUser myUser;
        try {
            //выполнение запроса
            myUser = query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return myUser;
    }

    @Override
    public MyUser getById(Long id) {
        return sessionFactory.getCurrentSession().get(MyUser.class, id);
    }

    @Override
    public boolean create(MyUser myUser) {
        Session session = sessionFactory.getCurrentSession();
        session.save(myUser);
        return true;
    }

    @Override
    public boolean update(MyUser myUser) {
        Session session = sessionFactory.getCurrentSession();
        session.update(myUser);
        return true;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public List<MyUser> getAll() {
        return null;
    }
}
