package com.dao;


import com.entity.UserAppeal;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppealDao implements GenericDao<UserAppeal, Long> {

    private SessionFactory sessionFactory;

    @Autowired
    public AppealDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean create(UserAppeal userAppeal) {
        Session session = sessionFactory.getCurrentSession();
        session.save(userAppeal);
        return true;
    }

    @Override
    public List<UserAppeal> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select u from UserAppeal u ", UserAppeal.class).getResultList();
    }

    @Override
    public UserAppeal getById(Long id) {
        return sessionFactory.getCurrentSession().get(UserAppeal.class, id);
    }

    @Override
    public boolean update(UserAppeal userAppeal) {
        Session session = sessionFactory.getCurrentSession();
        session.update(userAppeal);
        return true;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    public Integer getLastId() {
        Session session = sessionFactory.getCurrentSession();
        Integer lastId = (Integer) session.createSQLQuery("SELECT max(id) FROM appeal").uniqueResult();
        return lastId;
    }
}
