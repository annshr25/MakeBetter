package com.dao;

import com.entity.ChatMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChatMessageDao {
    private SessionFactory sessionFactory;

    @Autowired
    public ChatMessageDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(ChatMessage chatMessage) {
        Session session = sessionFactory.getCurrentSession();
        session.save(chatMessage);
    }
}
