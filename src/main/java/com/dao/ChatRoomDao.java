package com.dao;

import com.entity.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ChatRoomDao extends JpaRepository<ChatRoom, Long> {

    @Query("select c from ChatRoom c where (c.senderId = ?1 and c.recipientId = ?1) or (c.recipientId = ?1 and c.senderId = ?2)")
    List<ChatRoom> findBySenderIdAndRecipientId(String senderId, String recipientId);
}
