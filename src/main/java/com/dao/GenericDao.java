package com.dao;

import java.util.List;

public interface GenericDao<Entity, Key> {
    List<Entity> getAll();

    Entity getById(Key id);

    boolean update(Entity entity);

    boolean delete(Key id);

    boolean create(Entity entity);
}
