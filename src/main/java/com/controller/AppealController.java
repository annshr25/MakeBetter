package com.controller;

import com.dto.AppealDto;
import com.service.AppealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
public class AppealController {
    private AppealService appealService;

    @Autowired
    public AppealController(AppealService appealService) {
        this.appealService = appealService;
    }

    @GetMapping("/appeal_form")
    public String setAppealForm(Model model) {
        AppealDto appealDto = new AppealDto();
        appealDto.setAppealId(appealService.getLastId() + 1);
        model.addAttribute("appealForm", appealDto);
        return "appeal_form";
    }

    @PostMapping("/appeal_form")
    public String addNewAppeal(@ModelAttribute("appealForm") @Valid AppealDto appealDto,
                               BindingResult bindingResult, Model model) {
        appealService.addAppeal(appealDto);
        //TODO обработка данных формы
        return "redirect:/";
    }

    @GetMapping("/map")
    public String getMapPage() {
        return "map";
    }

}
