package com.controller;

import com.dto.ChatDto;
import com.entity.ChatMessage;
import com.entity.ChatNotification;
import com.entity.MyUser;
import com.service.ChatMessageService;
import com.service.ChatRoomService;
import com.service.CustomUserDetailsService;
import com.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.messaging.simp.SimpMessagingTemplate;


@Controller
public class ChatController {

    private RegistrationService registrationService;
    private ChatRoomService chatRoomService;
    private SimpMessagingTemplate messagingTemplate;
    private ChatMessageService chatMessageService;
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public ChatController(RegistrationService registrationService, ChatRoomService chatRoomService,
                          ChatMessageService chatMessageService, SimpMessagingTemplate messagingTemplate,
                          CustomUserDetailsService customUserDetailsService) {
        this.registrationService = registrationService;
        this.chatRoomService = chatRoomService;
        this.chatMessageService = chatMessageService;
        this.messagingTemplate = messagingTemplate;
        this.customUserDetailsService = customUserDetailsService;
    }

    @GetMapping("/chat/{id}")
    public String openChat(@PathVariable("id") String id, Model model) {
        String[] arrId = id.split("_");
        MyUser sender = customUserDetailsService.getCurrentUser();
        MyUser recipient;

        if (Long.valueOf(arrId[0]).equals(sender.getId())) {
            recipient = registrationService.findUserById(Long.valueOf(arrId[1]));
        } else {
            recipient = registrationService.findUserById(Long.valueOf(arrId[0]));
        }

        ChatDto chatDto = new ChatDto();
        chatDto.setRecipientName(recipient.getUsername());
        chatDto.setRecipientId(recipient.getId());

        chatDto.setSenderName(sender.getUsername());
        chatDto.setSenderId(sender.getId());

        model.addAttribute("chatForm", chatDto);
        return "/chat";
    }

    @MessageMapping("/chat")
    public void processMessage(@Payload ChatMessage chatMessage) {
        String chatId = chatRoomService
                .getChatId(chatMessage.getSenderId(), chatMessage.getRecipientId(), true);
        chatMessage.setChatId(chatId);

        ChatMessage saved = chatMessageService.save(chatMessage);

        messagingTemplate.convertAndSendToUser(
                chatMessage.getRecipientId(), "/queue/messages",
                new ChatNotification(
                        saved.getId(),
                        saved.getSenderId(),
                        saved.getSenderName(),
                        saved.getContent()));
    }

}
