package com.controller;

import com.dto.MapDto;
import com.google.gson.Gson;
import com.service.AppealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
public class MapController {
    private AppealService appealService;

    @Autowired
    public MapController(AppealService appealService) {
        this.appealService = appealService;
    }

    @GetMapping("/data_json")
    public String getJson() {
        Gson gson = new Gson();
        String json = gson.toJson(appealService.getAppeals());
        return json;
    }


    @PostMapping("/rating")
    public void getRating(@RequestBody MapDto mapdto) {
        appealService.updateRating(mapdto);
    }
}
