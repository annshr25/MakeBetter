package com.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertiesItem {
    @JsonProperty("balloonContentHeader")
    private String balloonContentHeader;//id
    @JsonProperty("balloonContentBody")
    private String balloonContentBody;//description
    @JsonProperty("balloonContentFooter")
    private String balloonContentFooter;
    @JsonProperty("clusterCaption")
    private String clusterCaption;
    @JsonProperty("hintContent")
    private String hintContent;
    @JsonProperty("idUser")
    private Long idUser;
    @JsonProperty("idCurrent")
    private Long idCurrent;
}
