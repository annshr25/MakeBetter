package com.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Response {
    @JsonProperty("type")
    private final String type = "FeatureCollection";
    @JsonProperty("features")
    private List<JsonItem> features;

    public Response() {
        features = new ArrayList<>();
    }
}
