package com.entity;

public enum MessageStatus {
    RECEIVED, DELIVERED
}
