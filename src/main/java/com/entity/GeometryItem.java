package com.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeometryItem {
    @JsonProperty("type")
    private final String type = "Point";
    @JsonProperty("coordinates")
    private double[] coordinates;
}
