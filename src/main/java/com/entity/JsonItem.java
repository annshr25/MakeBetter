package com.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonItem {
    @JsonProperty("type")
    private final String type = "Feature";
    @JsonProperty("id")
    private Long id;
    @JsonProperty("geometry")
    private GeometryItem geometry;
    @JsonProperty("properties")
    private PropertiesItem properties;
}
