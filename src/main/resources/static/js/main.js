// Функция ymaps.ready() будет вызвана, когда
// загрузятся все компоненты API, а также когда будет готово DOM-дерево.
ymaps.ready(init);

function init() {
    // Создание карты.
    var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [51.67, 39.18],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 9,
            controls: ['smallMapDefaultSet']
        }, {
            // Поиск по организациям.
            searchControlProvider: 'yandex#search'
        }),
        /*
        * Определение шаблона балуна
        * */
        BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
            '<h2 class=balloon_header>Обращение №{{ properties.balloonContentHeader|raw }}</h2>' +
            '<div class=balloon_body>{{ properties.balloonContentBody|raw }}</div>' +
            '<div class=balloon_footer><i id="like_count">{{ properties.balloonContentFooter|raw }}</i> ' +
            '<span id = "id_user" style="display: none">{{ properties.idUser|raw }}</span>' +
            '<span id = "id_current_user" style="display: none">{{ properties.idCurrent|raw }}</span>' +
            '<button id="like_button">Поддержать</button><button id="chat_button">Чат</button></div>', {

                // Переопределяем функцию build, чтобы при создании макета начинать
                // слушать событие click на кнопке-счетчике.
                build: function () {
                    // Сначала вызываем метод build родительского класса.
                    BalloonContentLayout.superclass.build.call(this);
                    // А затем выполняем дополнительные действия.
                    $('#like_button').bind('click', this.onCounterClick);
                    $('#chat_button').bind('click', this.onChatClick);
                    var counter = document.getElementById("like_count").innerHTML;
                    $('#like_count').html(counter);
                },
                onCounterClick: function () {
                    var counter = document.getElementById("like_count").innerHTML;
                    counter++;
                    $('#like_count').html(counter);
                    this.disabled = 'disabled';
                },
                onChatClick: function () {
                    var idChatRoom = document.getElementById("id_user").innerHTML
                        +"_"+document.getElementById("id_current_user").innerHTML;
                    window.open('chat/' + idChatRoom, "_self");
                }
            });

    objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        }
    );

    objectManager.clusters.options.set('preset', 'islands#redClusterIcons');

    objectManager.objects.options.set({
        present: 'islands#redDotIcon',
        balloonContentLayout: BalloonContentLayout,
        balloonPanelMaxMapArea: 0,
        // Балун будем открывать и закрывать кликом по иконке метки.
        balloonOffset: [-15, -15],
        balloonCloseButton: false,
        hideIconOnBalloonOpen: false
    });

    myMap.geoObjects.add(objectManager);
    /*
    * Загрузка меток с сервера
    * */
    $.ajax({
        url: "http://localhost:8080/data_json"
    }).done(function (data) {
        objectManager.add(data);
    });
    /*
    * Сохранение новых значений рейтинга для отправки на сервер
    * */
    var map = {};
    map.idArray = [];
    map.ratingArray = [];

    function updateRating(objectId) {
        if (map.idArray.includes(objectId)) {
            var index = map.idArray.indexOf(objectId);
            map.ratingArray.splice(index, 1, document.getElementById("like_count").innerHTML);
        } else {
            map.ratingArray.push(document.getElementById("like_count").innerHTML);
            map.idArray.push(objectId);
        }
    }

    objectManager.objects.events.add('click', function (e) {
        var objectId = e.get('objectId'),
            object = objectManager.objects.getById(objectId);
        if (objectManager.objects.balloon.isOpen(objectId)) {
            object.properties = {
                balloonContentHeader: object.properties.balloonContentHeader,
                balloonContentBody: object.properties.balloonContentBody,
                balloonContentFooter: document.getElementById("like_count").innerHTML,
                idUser:object.properties.idUser
            };
            updateRating(objectId);
            // map.myMap.set(objectId,document.getElementById("like_count").innerHTML);
            objectManager.objects.balloon.setData(object);
            objectManager.objects.balloon.close();
        }
    });
    /*
    * отправка рейтинга на сервер при закрытии страницы
    * */
    $(window).on('unload', function () {
        if(map.idArray.length!=0){
            var headers = {type: 'application/json'};
            var blob = new Blob([JSON.stringify(map)], headers);

            window.navigator.sendBeacon("http://localhost:8080/rating", blob);
        }
    });
    /*
    * Переход к форме нового обращения
    * */
    var createButton =
        new ymaps.control.Button({
                data: {
                    content: "Создать обращение",
                },
                options: {
                    maxWidth: 150
                }
            }
        );
    createButton.events
        .add(
            'click',
            function () {
                window.open('appeal_form?&' + address + '&' + coords, "_self");
            }
        );
    myMap.controls.add(createButton, {
        float: "left"
    });
    /*
    * Получение адреcса и координат метки на карте
    * */
    myPlacemark = null;
    var coords = [0, 0];

    myMap.events.add('click', function (e) {
        coords = e.get('coords');

        // Если метка уже создана – просто передвигаем ее.
        if (myPlacemark != null) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            // Слушаем событие окончания перетаскивания на метке.
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }
        getAddress(coords);
    });

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }

    // Определяем адрес по координатам (обратное геокодирование).
    var address;

    function getAddress(coords) {
        myPlacemark.properties.set('iconCaption', 'поиск...');

        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            myPlacemark.properties
                .set({
                    // Формируем строку с данными об объекте.
                    iconCaption: [
                        // Название населенного пункта или вышестоящее административно-территориальное образование.
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    ].filter(Boolean).join(', '),
                    // В качестве контента балуна задаем строку с адресом объекта.
                    balloonContent: firstGeoObject.getAddressLine()
                });
            address = firstGeoObject.getAddressLine();
        });

    }

}