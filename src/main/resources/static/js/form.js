function setCoord() {
    var coords = decodeURIComponent(location.search.substr(1)).split('&');
    var result = coords[2];
    document.getElementById("coords").setAttribute("size", result.length);
    document.getElementById("coords").value = result;
    var address = coords[1];
    document.getElementById("address").setAttribute("size", address.length);
    document.getElementById("address").value = address;
}

function addOption(oListbox, text, value, isDefaultSelected, isSelected) {
    var oOption = document.createElement("option");
    oOption.appendChild(document.createTextNode(text));
    oOption.setAttribute("value", value);

    if (isDefaultSelected) oOption.defaultSelected = true;
    else if (isSelected) oOption.selected = true;

    oListbox.appendChild(oOption);
}

function setCategory() {
    var objSel = document.getElementById("category");
    var categoryArr = ['Уборка мусора', 'Ремонт дороги', 'Грязный водоем', 'Уличное освещение'];
    for (var i = 0; i < categoryArr.length; i++) {
        addOption(objSel, categoryArr[i], categoryArr[i]);
    }
}

function setChat() {
    var messageForm = document.querySelector('#messageForm');
    var messageInput = document.querySelector('#message');
    var messageArea = document.querySelector('#messageArea');

    var stompClient = null;

    function connect() {
        var socket = new SockJS("http://localhost:8080/ws");
        stompClient = Stomp.over(socket);
        stompClient.connect({}, onConnected);
    }

    connect();

    function onConnected() {
        // Subscribe to the Public Topic
        stompClient.subscribe("/user/" + document.getElementById("sender_id").innerHTML + "/queue/messages",
            onMessageReceived);
        stompClient.subscribe("/user/" + document.getElementById("recipient_id").innerHTML + "/queue/messages",
            onMessageReceived);
    }

    function sendMessage(event) {
        var messageContent = messageInput.value.trim();
        if (messageContent && stompClient) {
            var currentUser = {
                id: document.getElementById("sender_id").innerHTML,
                name: document.getElementById("sender_name").innerHTML
            }
            var activeContact = {
                id: document.getElementById("recipient_id").innerHTML,
                name: document.getElementById("recipient_name").innerHTML
            }
            const message = {
                senderId: currentUser.id,
                recipientId: activeContact.id,
                senderName: currentUser.name,
                recipientName: activeContact.name,
                content: messageInput.value,
                timestamp: new Date(),
            };
            stompClient.send("/app/chat", {}, JSON.stringify(message));
            messageInput.value = '';
        }
        event.preventDefault();
    }

    function onMessageReceived(payload) {
        var message = JSON.parse(payload.body);
        console.log(message);

        var messageElement = document.createElement('li');

        messageElement.classList.add('chat-message');
        var usernameElement = document.createElement('strong');
        usernameElement.classList.add('nickname');
        var usernameText = document.createTextNode(message.senderName+" ");
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);

        var textElement = document.createElement('span');
        var messageText = document.createTextNode(message.content);
        textElement.appendChild(messageText);

        messageElement.appendChild(textElement);

        messageArea.appendChild(messageElement);
        messageArea.scrollTop = messageArea.scrollHeight;
    }

    messageForm.addEventListener('submit', sendMessage, true);
}



