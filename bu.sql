--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

-- Started on 2020-08-17 17:13:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2816 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 39448)
-- Name: appeal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appeal (
    id integer NOT NULL,
    category character varying NOT NULL,
    description character varying,
    coords character varying,
    img_path character varying,
    user_id bigint NOT NULL,
    address character varying,
    rating integer
);


ALTER TABLE public.appeal OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 39446)
-- Name: appeal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appeal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appeal_id_seq OWNER TO postgres;

--
-- TOC entry 2817 (class 0 OID 0)
-- Dependencies: 198
-- Name: appeal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appeal_id_seq OWNED BY public.appeal.id;


--
-- TOC entry 197 (class 1259 OID 31256)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id integer NOT NULL,
    username character varying(30) NOT NULL,
    password character varying(100) NOT NULL,
    role character varying(20)
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 31254)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- TOC entry 2818 (class 0 OID 0)
-- Dependencies: 196
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- TOC entry 2678 (class 2604 OID 39451)
-- Name: appeal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appeal ALTER COLUMN id SET DEFAULT nextval('public.appeal_id_seq'::regclass);


--
-- TOC entry 2677 (class 2604 OID 31259)
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- TOC entry 2808 (class 0 OID 39448)
-- Dependencies: 199
-- Data for Name: appeal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appeal (id, category, description, coords, img_path, user_id, address, rating) FROM stdin;
8	Уличное освещение	Нет света на улице	51.78427858626978,39.38324707031251	photo5316975547886185898.jpg	15	Россия, Воронежская область, Новоусманский район	1
9	Уборка мусора	Не убирается мусор	51.743376350736305,39.29810302734374	202007010936231000.jpg	15	Россия, Воронеж, Железнодорожный район	4
10	Грязный водоем	Надо почистить болото	51.641944117265524,39.16748395605268	photo5316975547886185898.jpg	15	Россия, Воронеж, Новый переулок, 12	7
\.


--
-- TOC entry 2806 (class 0 OID 31256)
-- Dependencies: 197
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, username, password, role) FROM stdin;
15	user	$2a$10$oy/Xtpi2.gwVN90XLkPu2.g8tXGhYGj1l5jLARn4/pgtXpZ0g95KC	USER
17	admin	$2a$10$6mwerf43VSKI0kxcODYDxuWNq1gRGgmd45pQlZHLYvwQkSyY5EKtW	ADMIN
21	ann	$2a$10$Js7SffyBISJ0PWZG22KJCeUzyW3AK4j334dfsjSo4ZZjde2/wv/0e	USER
\.


--
-- TOC entry 2819 (class 0 OID 0)
-- Dependencies: 198
-- Name: appeal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appeal_id_seq', 10, true);


--
-- TOC entry 2820 (class 0 OID 0)
-- Dependencies: 196
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 21, true);


--
-- TOC entry 2680 (class 2606 OID 31261)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- TOC entry 2682 (class 2606 OID 39456)
-- Name: appeal pk_appeal; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appeal
    ADD CONSTRAINT pk_appeal PRIMARY KEY (id);


--
-- TOC entry 2683 (class 2606 OID 39462)
-- Name: appeal fk_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appeal
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2020-08-17 17:14:00

--
-- PostgreSQL database dump complete
--

